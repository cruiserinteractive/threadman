/* $Id$ $URL$ */
#ifndef SEMWAITER_DOT_AITCH
#define SEMWAITER_DOT_AITCH

/**\file semwaiter.h
 * Declaration of SemWaiter
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */

#include <SDL_thread.h>
#include <SDL_version.h>

/** A simple C++ Sempahore wrapper */
class SemWaiter {
    /** Assignment is disabled */
    SemWaiter& operator=(const SemWaiter&);
protected:
    SDL_sem *sem;      ///< The SDL Semaphore we are wrapping
    volatile bool run; ///< Whether we keep running
    Uint32 initial_resources; ///< How many resources are available for copies
public:
    /** Constructor creates the semaphore and inits data members */
    SemWaiter (const SemWaiter& rhs)
        :
    sem(SDL_CreateSemaphore(rhs.initial_resources)), run(true), initial_resources(rhs.initial_resources) {}

    /** Create a Semaphore, initially set to have \a "resources" */
    explicit SemWaiter(Uint32 resources = 0)
        :
    sem(SDL_CreateSemaphore(resources)), run(true), initial_resources(resources) {}

    /** Destroy the Semaphore */
    ~SemWaiter() {SDL_DestroySemaphore(sem); sem = 0; run = false;}

    /** Post to the Semaphore -- increment it -- notify next waiter that a resource is available */
    void post() {SDL_SemPost(sem);}

    /** Stop the SemWaiter -- don't allow further wait()s and post to (at most) 1 waiter waiting */
    void stop() {run = false; post();}

    /** Don't allow further wait()s to block, but don't post() */
    void cancel() {run = false;}

    /** Wrest control from another SemWaiter.
     * Normally called from an object's ~Destructor() like
     *\code
     * destroy.wrest(thingo_ready, &thingo_processor);
     *\endcode
     * Where the last act of the thingo_processor thread will be to call
     * stop().
     * \see DestroyWaiter
     */
    int wrest(SemWaiter &alt, SDL_Thread **thr, Uint32 timeout = 0) {
        alt.stop();
        return threadwait(thr, timeout);
    }
    /** Wait for a thread to terminate, if non-null */
    int threadwait(SDL_Thread **thr, Uint32 timeout = 0) {
        int r = -1;
        /* if \a thr is NULL, we assume the thread was never started,
         * and so there will be no response to post to something waiting
         * in that thread, hence no way for the next wait() to return
         */
        if (thr && *thr) {
            if (timeout) {
                if (!timedwait(true, timeout)) {
#if SDL_VERSION_ATLEAST(2, 0, 0)
                    SDL_WaitThread(*thr, &r);
#else
                    SDL_KillThread(*thr);
#endif
                    return -1;
                }
            } else {
                wait();
            }
            SDL_WaitThread(*thr, &r);
            *thr = 0;
        }

        return r;
    }
    /**
     * Wait for this semaphore -- decrease available resources.
     *
     * \param spin If true we keep trying SemWait until it returns success (when debugging
     *        for example, it is common for the system call to get interrupted here)
     */
    bool wait(bool spin = true) {
        bool b = false;
        while (run && (b = (SDL_SemWait(sem) == -1)) && spin)
            ; //spin ==> !b || !run
        return run && !b;
    }
    bool timedwait(bool spin = true, Uint32 timeout = 0) {
        bool b = false;
        while (run && (b = (SDL_SemWaitTimeout(sem, timeout) == -1)) && spin)
            ; //spin ==> !b || !run
        if (b == SDL_MUTEX_TIMEDOUT) {
            /* timed out : b != 0 -> return false*/
        }
        return run && !b;
    }
    /** Overloaded Wait calls threadwait() */
    int wait(SDL_Thread **thr, Uint32 timeout = 0) {return threadwait(thr, timeout);}

    /** HAS SIDEFFECTS: returns wait() if we are running or false */
    operator bool() {return run ? wait() : false;}

    /** Return true if we are running */
    bool running() const {return run;}

    //virtual ~SemWaiter() {}
};

/** A SemWaiter for handling object destruction */
class DestroyWaiter : private SemWaiter {
    enum {DESTROYED_SEMVALUE = 2};
public:
    /** Creates a SemWaiter with a single resource */
    DestroyWaiter() : SemWaiter(DESTROYED_SEMVALUE) {
        while (SDL_SemValue(sem) != 1)
            SDL_SemWait(sem);
    }

    /**
     * Indicate that we want to destroy X
     *
     * \param alternate A SemWaiter that \a thr might be wait()ing on
     * \param thr An optional thread; wait for this to complete after
     *        stopping \a alternate
     * \param timeout Timeout in milliseconds if we need to wrest control from the thread
     */
    int wants_to_destroy(SemWaiter &alternate, SDL_Thread **thr, Uint32 timeout = 0) {
        if (!running())
            return true;
        return wrest(alternate, thr, timeout);
    }

    /**
     * Indicate that we want to destroy X
     *
     * \param thr An optional thread that we will wait for completion
     * \param timeout timeout for waits, in milliseconds
     */
    int wants_to_destroy(SDL_Thread **thr = 0, Uint32 timeout = 0) {
        int r = -1;

        if (timeout) {
            if (!timedwait(true, timeout) && thr && *thr) {
                /* probably timed out */
#if SDL_VERSION_ATLEAST(2, 0, 0)
                SDL_WaitThread(*thr, &r);
#else
                SDL_KillThread(*thr);
#endif
                *thr = 0;
            }
            cancel();
        } else {
            cancel();
            while (SDL_SemWait(sem) != 0)
                ;
            SDL_SemPost(sem); // never let it stay at 0
        }
        if (thr && *thr) {
            SDL_WaitThread(*thr, &r);
            *thr = 0;
        }
        /* there is a race condition here, but we don't really care */
        while (SDL_SemValue(sem) < DESTROYED_SEMVALUE)
            SDL_SemPost(sem);
        return r;
    }

    /**
     * Indicate to the thread that called wants_to_destroy() that X is
     * now ready to destroy
     */
    void ready_to_destroy() {
        stop();
    }

    /**
     * Return true if this sem has been destroyed
     */
    bool hasDestroyed() {
        return !run || SDL_SemValue(sem) >= DESTROYED_SEMVALUE;
    }

    /** Return the value of the semaphore */
    Uint32 value() {
        return SDL_SemValue(sem);
    }

    /**
     * Cause wants_to_destroy to block -- the alternate / thr has started
     */
    bool alternate_started() {
        return wait();
    }

    /** Return true if we are running */
    bool running() const {return SemWaiter::running();}
    void cancel() {SemWaiter::cancel();}
};

#endif
