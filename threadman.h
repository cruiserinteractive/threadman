/* $Id$ $URL$ */
#ifndef THREADMAN_DOT_AITCH
#define THREADMAN_DOT_AITCH

/**\file threadman.h
 * Declaration of ThreadMan
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */

#ifdef _MSC_VER
#ifdef THREADMAN_EXPORTING
#define THREADMAN_API __declspec(dllexport)
#else
#define THREADMAN_API __declspec(dllimport)
#endif
#else
#define THREADMAN_API //nothing
#endif

#if defined DEBUG_STL && defined __GNUC__ && __GNUC__ >= 4
#undef _GLIBCXX_VECTOR
#include <debug/vector>
#if __GNUC_MINOR__ >= 2
namespace stl = std::__debug;
#else
namespace stl = __gnu_debug_def;
#endif
#else
#include <vector>
namespace stl = std;
#endif

#include <SDL_thread.h>
#include <string>

class SyncThreads;

/** Singleton Manager for SDL Threads that we want to detach */
class THREADMAN_API ThreadMan {
public:
    ///\name Runner classes, for more flexible thread starting -- work in progress
    //@{
    /** Superclass runner */
    struct Runner {
        bool cleanup;
        explicit Runner(bool clean) : cleanup(clean) {}
        virtual int run(void* data) = 0;
        virtual bool operator==(const Runner &rhs) const = 0;
        virtual ~Runner() {}
    };
    /** Functor runner, taking a void* argument, returning an int */
    template <class F>
        struct F1Runner : public Runner {
            F f;
            F1Runner(const F& ff, bool clean = false) : Runner(clean), f(ff) {}
            virtual int run(void* data) {return f(data);}
            virtual bool operator==(const Runner &rhs) const {
                const F1Runner *r = dynamic_cast<const F1Runner*>(&rhs);
                return r && r->f == f;
            }
        };
    /** Functor runner, taking a no argument, returning an int */
    template <class F>
        struct F0Runner : public Runner {
            F f;
            F0Runner(const F& ff, bool clean = false) : Runner(clean), f(ff) {}
            virtual int run(void* data) {return f();}
            virtual bool operator==(const Runner &rhs) const {
                const F0Runner *r = dynamic_cast<const F0Runner*>(&rhs);
                return r && r->f == f;
            }
        };
    /** Member function runner -- taking no argument (data is `this`), returning an int */
    template <class T, class Ret>
    struct MFRunner : public Runner {
        Ret (T::*f)();
        MFRunner(Ret (T::*mf)(), bool clean = false) : Runner(clean), f(mf) {}
        virtual int run(void* data) {return (static_cast<T*>(data)->*f)() ;}
        virtual bool operator==(const Runner &rhs) const {
            const MFRunner *r = dynamic_cast<const MFRunner*>(&rhs);
            return r && r->f == f;
        }
    };
    /** Match the member function runner */
    template <class T, class Ret>
    static Runner* runner(Ret (T::*func)() ) {
        return new MFRunner<T, Ret>(func);
    }
    //@}

    typedef int (*THREAD)(void*); ///< Type for threads

    /** Thread priorities. Note we do not support high priority.
     * Note also that on Windows, these are relative to the parent while
     * on POSIX they are absolute.
     */
    enum PRIORITY {
        INHERIT,      ///< Inherit from parent process
        NORMAL,       ///< Normal or nice(0)
        BELOW_NORMAL, ///< Below Normal or nice(5)
        LOW,          ///< Low or nice (10)
        IDLE          ///< Idle of nice(19)
    };

    /** Set the priority of *this* thread */
    static void setMyPriority(PRIORITY pri);
        
    /** Set the name of *this* thread */
    static void setThreadName(const char *name);
        
private:
    /** Function data */
    struct FnData {
        THREAD fn;            ///< The *actual* thread entry point
        void * data;          ///< Data passed to the thread by \a run()
        SDL_Thread *thr;      ///< Handle on the thread
        SDL_sem *startup_sem; ///< Semaphore to indicate to thread that it can call thread_done
        ThreadMan *tman;      ///< Constructor of this data
        PRIORITY pri;         ///< The priority to set the thread to, once started
        std::string name;     ///< Name of the thread to set (if implemented on the current platform)

        /** Constructor creates/starts the thread and posts to the semaphore */
        FnData (THREAD f, void* d, ThreadMan *tm, PRIORITY pri, const char *name);
        /** Destructor just nukes the semaphore */
        ~FnData() {if (startup_sem) SDL_DestroySemaphore(startup_sem);}
        void disown() {SDL_SemPost(startup_sem);}
    private:
        FnData(const FnData&);
        FnData& operator=(const FnData&);
    };
    /** Cleanup struct -- ThreadMan repeatedly calls done(); to ask if it is finished*/
    struct ThreadKiller {
        SDL_Thread *thr;      ///< Handle on the thread
        bool finished;        ///< Set to true by the thread, when entry point returns

        /** Cleanup the thread, if we know we won't block, and return true */
        bool done() {
            int ignored;
            if (finished)
                SDL_WaitThread(thr, &ignored);
            return finished;
        }

        int wait();

        /** Constructor just copies the thread handle */
        ThreadKiller(SDL_Thread *t) : thr(t), finished(false) {}
        /** Compare == if we handle the same thread */
        bool operator==(SDL_Thread *t) {return thr == t;}
    };

    typedef stl::vector<ThreadKiller> THREADS; ///< type for managed threads

    THREADS threads;      ///< Managed threads
    SDL_mutex *thrmut;    ///< Mutex for accessing \a threads
    SyncThreads *syncman; ///< Synchronised threads

    /** Check for finished threads to Wait/Join */
    void thread_cleanup();

    /** Indicate that we can now call SDL_WaitThread(\a t) without blocking */
    void thread_done(SDL_Thread *t);

    /** Function passed to SDL_CreateThread */
    static int run_detached (void *fndata);

    /** Add a killer for \a t */
    void thread_add(SDL_Thread *t);

    ThreadMan& operator=(const ThreadMan& rhs); ///< No Assignment
    ThreadMan(const ThreadMan& rhs);            ///< No copy construction
public:
    /** Constructor -- nothing interesting */
    ThreadMan();
    /** Destructor -- also not interesting */
    ~ThreadMan();

    ///\name Member Functions for running threads
    //@{
    /**
     * Run a thread and forget about it, to be cleaned on the next call to run(), if it has finished.
     *
     * \param fn The thread to run (like that passed to SDL_CreateThread)
     * \param data The data to pass to the thread
     * \param sync If true, use synchronised mode. In synchronised mode, only one thread exists for each distinct
     *             \a fn and we reuse it when each call finishes, rather than cleaning up. stopSync() will stop
     *             all threads started in this fashion (waiting for \a fn to finish if it is currently running).
     * \param pri The thread priority
     * \param thr if non-null this is assigned to the SDL_Thread that will (eventually) run the function
     */
    bool run(THREAD fn, void* data = 0, bool sync = false, PRIORITY pri = INHERIT, const char *name = "", SDL_Thread **thr = 0);
    bool run(Runner *fn, void* data = 0, bool sync = false, PRIORITY pri = INHERIT, const char *name = "", SDL_Thread **thr = 0);
    //@}

    /** Stop all synchronised threads */
    void stopSync();
    /** Wait for all synchronised threads to finish */
    void joinSync();

    /** Stop asynchronous threads, wait for them to finish */
    void stopAsync();

    /** Static copy of thread manager */
    static ThreadMan *threadman;

    /** Initialise static variables (e.g. at application start-up) -- just calls static_reset() */
    static void static_init();

    /** Reset static variables (e.g. at application start-up) */
    static void static_reset();

    /** Clean-up static variables (e.g. at application shutdown) */
    static void static_cleanup();

    /** Return a reference to the singleton, creating it if it doesn't exist yet */
    static ThreadMan& tman() {return threadman ? *threadman : *(threadman = new ThreadMan()); }

    /**\name Static version of run() */
    //@{
    static bool srun(THREAD fn, void* data = 0, bool sync = false, PRIORITY pri = INHERIT, const char *name = "", SDL_Thread **thr = 0) {return tman().run(fn, data, sync, pri, name, thr);}
    static bool srun(Runner *fn, void* data = 0, bool sync = false, PRIORITY pri = INHERIT, const char *name = "", SDL_Thread **thr = 0) {return tman().run(fn, data, sync, pri, name, thr);}
    //@}
};

#endif
